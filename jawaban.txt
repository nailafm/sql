Soal 1 Membuat Database
create database myshop;

Soal 2 Membuat Table di Dalam Database

Table Users
create table users(
    -> id integer primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );

Table Categories
create table categories(
    -> id integer primary key auto_increment,
    -> name varchar(255)
    -> );

Table Items
create table items(
    -> id integer primary key auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price integer,
    -> stock integer,
    -> category_id integer,
    -> foreign key(category_id) references categories(id)
    -> );

Soal 3 Memasukan Data pada Table

Table Users
INSERT INTO users (name, email, password) VALUES('John Doe', 'john@doe.com', 'john123'); 
INSERT INTO users (name, email, password) VALUES('Jane Doe', 'jane@doe.com', 'jenita123');

Table Categories
INSERT INTO categories (name) VALUES("gadget"),("cloth"),("men"),("women"),("branded");

Table Items
INSERT INTO items(name, description, price, stock, category_id) VALUES(�Sumsang b50�, �hape keren dari merek sumsang�, 4000000, 100, 1), (�Uniklooh�, �baju keren dari brand ternama�, 500000, 50, 2), (�IMHO Watch�, �jam tangan anak yang jujur banget�, 2000000, 10, 1);


Soal 4 Mengambil Data dari Database
-mengambil data users kecuali password
 select id,name,email from users;

-mengambil data items dengan kondisi > 1000000
 select * from items where price>1000000;

-mengambil data menggunakan like
 select * from items where name like 'uniklo%';

-mengambil data di kedua table menggunakan join
 select items.id, items.name, items.description, items.price, items.stock, items.category_id, categories.name from items inner join categories on items.category_id = categories.id;

Soal 5 Mengubah Data dari Database
update items set price = 2500000 where id = 1;